import { Application, Router } from "https://deno.land/x/oak/mod.ts"

const port = parseInt(Deno.env.get("PORT") || "3000")
const router = new Router()
const app = new Application()

router.get("/", (ctx) => {
  ctx.response.status = 200
  ctx.response.headers.set("Content-Type", "text/plain")
  ctx.response.body = "Hello, World!"
})

router.get("/json", async (ctx) => {
  try {
    const fileContent = await Deno.readTextFile("./public/example.json")
    const jsonData = JSON.parse(fileContent)

    ctx.response.status = 200
    ctx.response.headers.set("Content-Type", "application/json")
    ctx.response.body = jsonData
  } catch (error) {
    ctx.response.status = 404
    ctx.response.body = "File not found"
  }
})

app.use(router.routes())
app.use(router.allowedMethods())

console.log(`Deno Oak server is running on http://localhost:${port}`)
await app.listen({ port })
