import { Server } from "https://deno.land/std@0.204.0/http/server.ts"

const port = parseInt(Deno.env.get("PORT") || "3000")

const handler = async (request: Request): Promise<Response> => {
  const url = new URL(request.url)

  if (url.pathname === "/") {
    return new Response("Hello, World!", { status: 200, headers: { "Content-Type": "text/plain" } })
  } else if (url.pathname === "/json") {
    try {
      // Asynchronously read the JSON file
      const fileContent = await Deno.readTextFile("./public/example.json")
      return new Response(fileContent, {
        status: 200,
        headers: { "Content-Type": "application/json" },
      })
    } catch (error) {
      return new Response("File not found", { status: 404 })
    }
  } else {
    return new Response("Not found", { status: 404 })
  }
}

const server = new Server({ handler })
const listener = Deno.listen({ port: port })

console.log(`Server listening on http://localhost:${port}`)

await server.serve(listener)
