const port = process.env.PORT || 3000

const server = Bun.serve({
  port: port,
  async fetch(request) {
    const url = new URL(request.url)

    if (url.pathname === "/") {
      return new Response("Hello, World!", {
        status: 200,
        headers: {
          "Content-Type": "text/plain",
        },
      })
    } else if (url.pathname === "/json") {
      const file = Bun.file("./public/example.json")
      return new Response(file)
    } else {
      return new Response("Not Found", {
        status: 404,
        headers: {
          "Content-Type": "text/plain",
        },
      })
    }
  },
})

console.log(`Bun server is running on http://localhost:${server.port}/`)
