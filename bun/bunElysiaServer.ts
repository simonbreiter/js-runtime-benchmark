import { Elysia, Context } from "elysia"

const port = process.env.PORT || 3000
const app = new Elysia()

app.get("/", (ctx: Context) => {
  ctx.set.status = 200
  ctx.set.headers = {
    "Content-Type": "text/plain",
  }
  return "Hello, World!"
})

app.get("/json", (ctx: Context) => {
  const file = Bun.file("./public/example.json")
  return file
})

app.listen(port)

console.log(`Bun Elysia is running on http://localhost:${port}...`)
