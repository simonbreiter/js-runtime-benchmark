const concurrently = require("concurrently")

async function startServers() {
  try {
    const commands = [
      { command: "PORT=3001 bun bun/bunServer.ts", name: "bun", prefixColor: "green" },
      {
        command: "PORT=3002 bun bun/bunExpressServer.ts",
        name: "bun-express",
        prefixColor: "green",
      },
      {
        command: "PORT=3003 bun bun/bunElysiaServer.ts",
        name: "bun-elysia",
        prefixColor: "green",
      },
      {
        command: "PORT=3004 deno run --allow-env --allow-net --allow-read deno/denoServer.ts",
        name: "deno",
        prefixColor: "yellow",
      },
      {
        command: "PORT=3005 deno run --allow-env --allow-net --allow-read deno/denoOakServer.ts",
        name: "deno-oak",
        prefixColor: "yellow",
      },
      { command: "PORT=3006 node --experimental-modules node/nodeServer.js", name: "node", prefixColor: "red" },
      {
        command: "PORT=3007 node --experimental-modules node/nodeExpressServer.js",
        name: "node-express",
        prefixColor: "red",
      },
      {
        command: "PORT=3008 node --experimental-modules node/nodeFastifyServer.js",
        name: "node-fastify",
        prefixColor: "red",
      },
    ]

    await concurrently(commands, {
      prefix: "name",
      killOthers: ["failure", "success"],
    })

    console.log("All servers started successfully")
  } catch (error) {
    console.error("Failed to start all servers: ", error)
  }
}

startServers()
