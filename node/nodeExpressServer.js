import express from "express"
import path from "path"
import { fileURLToPath } from "url"

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

const app = express()
const port = process.env.PORT || 3000

app.get("/", (req, res) => {
  res.set("Content-Type", "text/plain")
  res.status(200)
  res.send("Hello, World!")
})

app.get("/json", (req, res) => {
  res.sendFile(path.join(__dirname, "../public/example.json"))
})

app.listen(port, () => {
  console.log(`Express server is running on http://localhost:${port}`)
})
