import fs from "fs"
import Fastify from "fastify"

const port = process.env.PORT || 3000
const fastify = Fastify({
  logger: false,
})

fastify.get("/", async (request, reply) => {
  return { hello: "world" }
})

fastify.get("/json", async (request, reply) => {
  const stream = fs.createReadStream("./public/example.json")
  reply.header("Content-Type", "application/json")
  return reply.send(stream).type("application/pdf").code(200)
})

const start = async () => {
  try {
    console.log(`Node Fastify is running on http://localhost:${port}`)
    await fastify.listen({ port: port })
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}

start()
