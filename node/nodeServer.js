import http from "http"
import { readFile } from "fs" // Corrected import
import { join } from "path"
import { fileURLToPath } from "url"

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

const port = process.env.PORT || 3000

const server = http.createServer((req, res) => {
  if (req.url === "/") {
    res.writeHead(200, { "Content-Type": "text/plain" })
    res.end("Hello, World!")
  } else if (req.url === "/json") {
    readFile(join(__dirname, "../public/example.json"), (err, data) => {
      if (err) {
        res.writeHead(404, { "Content-Type": "text/plain" })
        res.end("Not Found")
      }

      res.writeHead(200, { "Content-Type": "application/json" })
      res.end(data)
    })
  }
})

server.listen(port, () => {
  console.log(`Node server is running on http://localhost:${port}/`)
})
