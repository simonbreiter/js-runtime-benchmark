# js-runtime-benchmark

## Install

Make sure that node, deno and bun is installed. Then run `npm run setup` to install all dependencies.

## Usage

Start all servers at once with `npm start` or start them individually manual. All of them will expose the same API:

- `/` return a plain text `Hello, World!`
- `/json` read a JSON file from the file system and serve it
